const lineTable = document.querySelector("#table-father");
const buttonForm = document.querySelector("#add-book");
const inputForm = document.querySelectorAll("#add-book input");


function get() {
    fetch('https://treinamento-api.herokuapp.com/books')
    .then( parseRequest => parseRequest.json())
    .then(responseAsJson => {
        for (let index  = 0; index  < responseAsJson.length; index ++) {
            let sectionTable = document.createElement("tr");

            let tableName = document.createElement("td");
            let tableAutor = document.createElement("td");
            let tableCreated = document.createElement("td");
            let tableUpdated = document.createElement("td"); 
            let tableID = document.createElement("td");

            let tableButton = document.createElement("button");
            
            tableID.classList.add("id");
            tableName.classList.add("td");
            tableAutor.classList.add("td");
            tableCreated.classList.add("td");
            tableUpdated.classList.add("td");
            tableButton.classList.add("button");
            
            tableID.innerText = responseAsJson[index].id;
            tableName.innerText = responseAsJson[index].name;
            tableAutor.innerText = responseAsJson[index].author;
            tableCreated.innerText = responseAsJson[index].created_at;
            tableUpdated.innerText = responseAsJson[index].updated_at;
            
            tableButton.innerText = "excluir";

            sectionTable.appendChild(tableName);
            sectionTable.appendChild(tableAutor);
            sectionTable.appendChild(tableCreated);
            sectionTable.appendChild(tableUpdated);
            sectionTable.appendChild(tableButton);
            sectionTable.appendChild(tableID);

            lineTable.appendChild(sectionTable);
        }
    })
    .catch( reject => console.log(`Erro , ${reject}`));
}

function post(name,author){
    const newBook = {
        "book":{
            "name" : name,
            "author" : author
            }
        }
        const myHeaders = {
            "Content-Type" : "application/json"
        }
        
        const fetchConfig = {
            method:'POST',
            headers: myHeaders,
            body: JSON.stringify(newBook)
        }

        fetch('https://treinamento-api.herokuapp.com/books',fetchConfig)
        .then( parseRequest => parseRequest.json())
        .then(responseAsJson => {
            let sectionTable = document.createElement("tr");

            let tableName = document.createElement("td");
            let tableAutor = document.createElement("td");
            let tableCreated = document.createElement("td");
            let tableUpdated = document.createElement("td");
            
            tableName.classList.add("td");
            tableAutor.classList.add("td");
            tableCreated.classList.add("td");
            tableUpdated.classList.add("td");
            

            tableName.innerText = responseAsJson.name;
            tableAutor.innerText = responseAsJson.author;
            tableCreated.innerText = responseAsJson.created_at;
            tableUpdated.innerText = responseAsJson.updated_at;
        
            
            sectionTable.appendChild(tableName);
            sectionTable.appendChild(tableAutor);
            sectionTable.appendChild(tableCreated);
            sectionTable.appendChild(tableUpdated);

            lineTable.appendChild(sectionTable);
        })
        .catch( reject => console.log(`Erro , ${reject}`))
}

function deleteLineByID(id) {
      fetch(`https://treinamento-api.herokuapp.com/books/${id}`,{
          method:'DELETE'
      }).then((resolve)=>{
            console.log("excluir");
      }).catch((error)=>{
          alert(error);
     })
}




buttonForm.addEventListener("click",(e)=> {
    e.preventDefault();
    if(e.target.nodeName === "BUTTON") {
        let name = inputForm[0].value;
        let author = inputForm[1].value;
        post(name,author);
    }
});


lineTable.addEventListener("click", (e)=>{
    e.preventDefault();
    if(e.target.nodeName === "BUTTON"){
        e.target.parentNode.querySelector(".id");
        deleteLineByID(e.target.parentNode.querySelector(".id ").innerText);
        e.target.parentNode.remove();
    }
});


get();
